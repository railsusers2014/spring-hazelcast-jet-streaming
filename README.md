# Hazelcast JET running on Spring Boot #

[Hazelcast JET](https://blog.hazelcast.com/introducing-hazelcast-jet/) is currently new player on the field of the frameworks for distributed computations. According to Hazelcast team they're even faster then [Apache Spark](http://spark.apache.org/) and [Apache Flink](https://flink.apache.org/). See the [benchmark](https://hazelcast.com/wp-content/uploads/2017/01/PastedGraphic-1.png). Lets see how to use Hazelcast JET with [Spring Boot](https://projects.spring.io/spring-boot/) and of course as usual I will build a simple demo.

### Hazelcast JET and Spring Boot dependencies settings ###

Lets add last Hazelcast JET SNAPSHOT into the Spring Boot project pom:

```
<dependency>
	<groupId>com.hazelcast.jet</groupId>
	<artifactId>hazelcast-jet</artifactId>
	<version>0.3.2-SNAPSHOT</version>
</dependency>
```

and lets try to create just a Hazelcast JET instance and shut it down eventually
just in order to verify that we're able to start Hazelcast JET correctly:

```
 try {
        JetInstance instance = Jet.newJetInstance();
 } finally {
        Jet.shutdownAll();
 }
```
unfortunately Hazelcast JET instance creation inside of Spring Boot application will crash with:

```
java.lang.NoSuchFieldError: JET
	at com.hazelcast.jet.impl.config.XmlJetConfigBuilder.getXmlType(XmlJetConfigBuilder.java:128) ~[hazelcast-jet-0.3.2-SNAPSHOT.jar!/:0.3.2-SNAPSHOT]
	at com.hazelcast.config.AbstractXmlConfigHelper.getNamespaceType(AbstractXmlConfigHelper.java:136) ~[hazelcast-3.7.6.jar!/:3.7.6]
	at com.hazelcast.config.AbstractXmlConfigHelper.<init>(AbstractXmlConfigHelper.java:72) ~[hazelcast-3.7.6.jar!/:3.7.6]
	at com.hazelcast.config.AbstractConfigBuilder.<init>(AbstractConfigBuilder.java:59) ~[hazelcast-3.7.6.jar!/:3.7.6]
	at com.hazelcast.jet.impl.config.XmlJetConfigBuilder.<init>(XmlJetConfigBuilder.java:67) ~[hazelcast-jet-0.3.2-SNAPSHOT.jar!/:0.3.2-SNAPSHOT]
	at com.hazelcast.jet.impl.config.XmlJetConfigBuilder.getConfig(XmlJetConfigBuilder.java:80) ~[hazelcast-jet-0.3.2-SNAPSHOT.jar!/:0.3.2-SNAPSHOT]
```
Well, don't blame the Hazelcast JET team, it's just that the Spring Boot already comes with Hazelcast IMDG inside (version 3.7.6 in case of Spring Boot 1.5.3) where this version is packed with hazelcast *ConfigType enumeration which doesn't contain JET field*. And that's all. Workaround of this problem is to upgrade Hazelcast IMDG in your pom. What works for me is setting the **hazelcast.version** property to:

```
<properties>
	<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
	<java.version>1.8</java.version>
	<hazelcast.version>3.8</hazelcast.version>
</properties>
```

After this workaround, everything should work fine and you should be able to see their nice ascii art :-)

```
017-03-26 12:22:56.579  INFO 10934 --- [           main] com.hazelcast.system                     : [192.168.1.112]:5701 [jet] [0.3.2-SNAPSHOT] [3.8] Hazelcast 3.8 (20170217 - d7998b4) starting at [192.168.1.112]:5701
2017-03-26 12:22:56.580  INFO 10934 --- [           main] com.hazelcast.system                     : [192.168.1.112]:5701 [jet] [0.3.2-SNAPSHOT] [3.8] Copyright (c) 2008-2017, Hazelcast, Inc. All Rights Reserved.
2017-03-26 12:22:56.580  INFO 10934 --- [           main] com.hazelcast.system                     : [192.168.1.112]:5701 [jet] [0.3.2-SNAPSHOT] [3.8] Configured Hazelcast Serialization version : 1
2017-03-26 12:22:56.950  INFO 10934 --- [           main] c.h.s.i.o.impl.BackpressureRegulator     : [192.168.1.112]:5701 [jet] [0.3.2-SNAPSHOT] [3.8] Backpressure is disabled
2017-03-26 12:22:57.629  INFO 10934 --- [           main] com.hazelcast.instance.Node              : [192.168.1.112]:5701 [jet] [0.3.2-SNAPSHOT] [3.8] Creating MulticastJoiner
2017-03-26 12:22:57.860  INFO 10934 --- [           main] com.hazelcast.jet.impl.JetService        : [192.168.1.112]:5701 [jet] [0.3.2-SNAPSHOT] [3.8] Starting Jet 0.3.2-SNAPSHOT (20170324 - fd5868b) 
2017-03-26 12:22:57.860  INFO 10934 --- [           main] com.hazelcast.jet.impl.JetService        : [192.168.1.112]:5701 [jet] [0.3.2-SNAPSHOT] [3.8] Setting number of cooperative threads and default parallelism to 2
2017-03-26 12:22:57.860  INFO 10934 --- [           main] com.hazelcast.jet.impl.JetService        : [192.168.1.112]:5701 [jet] [0.3.2-SNAPSHOT] [3.8] 
	o   o   o   o---o o---o o     o---o   o   o---o o-o-o        o o---o o-o-o
	|   |  / \     /  |     |     |      / \  |       |          | |       |  
	o---o o---o   o   o-o   |     o     o---o o---o   |          | o-o     |  
	|   | |   |  /    |     |     |     |   |     |   |      \   | |       |  
	o   o o   o o---o o---o o---o o---o o   o o---o   o       o--o o---o   o   
2017-03-26 12:22:57.861  INFO 10934 --- [           main] com.hazelcast.jet.impl.JetService        : [192.168.1.112]:5701 [jet] [0.3.2-SNAPSHOT] [3.8] Copyright (c) 2008-2017, Hazelcast, Inc. All Rights Reserved.
2017-03-26 12:22:57.867  INFO 10934 --- [           main] c.h.s.i.o.impl.OperationExecutorImpl     : [192.168.1.112]:5701 [jet] [0.3.2-SNAPSHOT] [3.8] Starting 4 partition threads
2017-03-26 12:22:57.868  INFO 10934 --- [           main] c.h.s.i.o.impl.OperationExecutorImpl     : [192.168.1.112]:5701 [jet] [0.3.2-SNAPSHOT] [3.8] Starting 3 generic threads (1 dedicated for priority tasks)
2017-03-26 12:22:57.876  INFO 10934 --- [           main] com.hazelcast.core.LifecycleService      : [192.168.1.112]:5701 [jet] [0.3.2-SNAPSHOT] [3.8] [192.168.1.112]:5701 is STARTING
```

### Hazelcast JET architecture ###

Before going into the demo on JET lets describe its architecture. Hazelcast JET is based on the *composing your computation job into the [DAG graph](https://en.wikipedia.org/wiki/Directed_acyclic_graph)*. This isn't something revolutionary because Apache Spark works also based on DAG. But lets go a little deeper into the Hazelcast JET DAG terminology::

* **Vertex** One unit of work of your JET job. Take this as a step of your computation job. One Vertex contains one or more processors which you implement([class AbstractProcessor](https://github.com/hazelcast/hazelcast-jet/blob/master/hazelcast-jet-core/src/main/java/com/hazelcast/jet/AbstractProcessor.java)) to do the logic you want. You can have more processors per vertex. Number is based on the local and global parallelism settings. You can have **SINK** vertex (have only input, doesn't emit anything), **INTERNAL** vertex (has input and emits output to its ordinals) and **SOURCE** vertex (emits only).

* ** Edge ** Connector of your DAG vertices. Every time you create a JET job you have to connect your Processor implementations with Edges.

To show you an example lets build a simple JET Job which injects data from [RabbitMQ broker](https://www.rabbitmq.com/) into the distributed Hazelcast Map. Our job will contain two vertices, first vertex will emit items coming from RabbitMQ and second vertex is going to be simple Sink into the Map.

```
    private static Job createJetJob(JetInstance instance) {
        DAG dag = new DAG();
        Properties props = props(
                "server", "localhost",
                "user", "guest",
                "password", "guest");
        Vertex source = dag.newVertex("source", readRabbitMQ(props, "jetInputQueue"));
        Vertex sink = dag.newVertex("sink", writeMap("sink"));
        dag.edge(between(source, sink));
        return instance.newJob(dag);
    }
```

### Code in details ###

Method **readRabbitMQ** returns a JET Processor implementation for reading the messages from RabbitMQ. The implementation is inspired by official [ReadKafkaP processor](https://github.com/hazelcast/hazelcast-jet/blob/master/hazelcast-jet-kafka/src/main/java/com/hazelcast/jet/connector/kafka/ReadKafkaP.java). I have just rewritten the connector to speak to RabbitMQ broker in the polling manner. Messages reading part is in the *AbstractProcessor.complete* method. To understand this method you need to know two things:

* **Method is called repeatedly until true is returned**. Since first vertex in my job is a source then my ReadRabbitMQP.complete method always returns false. 
* Polled messages from RabbitMQ broker needs to be returned through [Traverser interface](https://github.com/hazelcast/hazelcast-jet/blob/master/hazelcast-jet-core/src/main/java/com/hazelcast/jet/Traverser.java). See also [Traversers](https://github.com/hazelcast/hazelcast-jet/blob/master/hazelcast-jet-core/src/main/java/com/hazelcast/jet/Traversers.java) util class for handy methods for this.

Complete method for polling the RabbitMQ messages:

```
    @Override
    public boolean complete() {
        System.out.println("....Invoking RabbitMQ vertex processor complete...");
        if (emitCooperatively(traverser)) {
            final Message message =
                    this.rabbitTemplate.receive(this.queueNames[0]);
            if (message != null) {
                System.out.println("Message payload: " + new String(message.getBody()));
                final List<Message> list = new ArrayList<>();
                list.add(message);
                Random rn = new Random();
                traverser = traverseStream(list.stream()).map(r ->
                        entry(String.valueOf(rn.nextInt()), new String(r.getBody()))
                );
            }
        }
        return false;
    }
```

### Hazelcast JET and work distribution into the cluster ###

When writing streaming from RabbitMQ broker into the Hazelcast JET then every procesor in the sink vertex is going to be a consumer, doing the same work. Payload from AMPQ queue is going to be load balanced among the JET processors. Anyway, the way how procesors are created for the particular vertices is the job of the [ProcessorMetaSupplier ](https://github.com/hazelcast/hazelcast-jet/blob/master/hazelcast-jet-core/src/main/java/com/hazelcast/jet/ProcessorMetaSupplier.java) and [ProcessorSupplier ](https://github.com/hazelcast/hazelcast-jet/blob/master/hazelcast-jet-core/src/main/java/com/hazelcast/jet/ProcessorSupplier.java )implementations. Just very briefly, JET uses ProcessorMetaSupplier implementation to get ProcessorSupplier for every vertex in the DAG graph. ProcessorSupplier is then being sent to the vertex which creates the Processors according the parallelism settings. I highly recommend to read the [NumberGenerator ](http://docs.hazelcast.org/docs/jet/0.3.1/manual/index.html#understanding-jet-architecture-and-api) example in the JET documentation which helps a lot to understand how JET creates the Processors.

In case of RabbitMQ streaming, I repeat, every Processor is going to be doing the same work, hence:

```
private static final class MetaSupplier<K, V> implements ProcessorMetaSupplier {

        static final long serialVersionUID = 1L;
        private final String[] queueNames;
        private Properties properties;

        private MetaSupplier(String[] queueNames, Properties properties) {
            this.queueNames = queueNames;
            this.properties = properties;
        }

        @Override
        public Function<Address, ProcessorSupplier> get(List<Address> addresses) {
            return address -> new Supplier<>(queueNames, properties);
        }
    }

    private static class Supplier<K, V> implements ProcessorSupplier {

        static final long serialVersionUID = 1L;

        private final String[] queueNames;
        private final Properties properties;
        private transient List<Processor> processors;

        Supplier(String[] topicIds, Properties properties) {
            this.properties = properties;
            this.queueNames = topicIds;
        }

        @Override
        public List<Processor> get(int count) {
            return processors = range(0, count)
                    .mapToObj(i -> new ReadRabbitMQP<>(queueNames, properties))
                    .collect(toList());
        }

        @Override
        public void complete(Throwable error) {
            processors.stream()
                    .filter(p -> p instanceof ReadRabbitMQP)
                    .map(p -> (ReadRabbitMQP) p)
                    .forEach(p -> Util.uncheckRun(p::close));
        }
    }
```

### Testing this demo ###

* git clone <this repo>
* mvn clean install
* create a queue inside of RabbitMQ with name "jetInputQueue"
* java -jar target/demo-0.0.1-SNAPSHOT.jar

Now put some text messages to "jetInputQueue". Size of sink map should be increased eventually.

(after insert of 4 messages output should be)
```
Received 4 entries in 78080 milliseconds.
....Invoking RabbitMQ vertex processor complete...
....Invoking RabbitMQ vertex processor complete...
Received 4 entries in 78182 milliseconds.
....Invoking RabbitMQ vertex processor complete...
....Invoking RabbitMQ vertex processor complete...
Received 4 entries in 78283 milliseconds.
....Invoking RabbitMQ vertex processor complete...
....Invoking RabbitMQ vertex processor complete...
Received 4 entries in 78385 milliseconds.
....Invoking RabbitMQ vertex processor complete...
....Invoking RabbitMQ vertex processor complete...
Received 4 entries in 78486 milliseconds.
....Invoking RabbitMQ vertex processor complete..
```

### My view on Hazelcast JET ###

Pros:

* Incredible performance. Even slightly better compare to Apache Spark.
* Very coding friendly DAG API. After just two days I was able to code JET DAG jobs.

Cons: 

* Better fault tolerance. Currently if my RabbitMQ JET consumer died then whole job is aborted with the data loss..:(
* I would like to see [Apache Hive](https://hive.apache.org/) support for Hazelcast JET. Like write SQL style statement and Hive would generate JET DAG for me like it works with Apache Spark.

Anyway JET has my attention and I'm looking forward to next versions.

regards

Tomas